# jump to root directory
cd ~

# install dependencies via apt 
sudo apt update
sudo apt install nodejs
sudo apt install npm

# install dependencies via npm
sudo npm install --global yarn
sudo npm install --global tldr

# install cURL
sudo apt-get install curl

# install PHP
sudo apt install php7.2-cli

# install composer (https://www.ionos.com/community/hosting/php/install-and-use-php-composer-on-ubuntu-1604/)
sudo curl -s https://getcomposer.org/installer | php
sudo mv composer.phar /usr/local/bin/composer

# install SQLite
sudo apt-get install php7.0-sqlite3

# gitconfig symlink
rm ~/.gitconfig
ln -s ~/config/gitconfig ~/.gitconfig

# vscode settings symlink
rm ~/.config/Code/User/settings.json
ln -s ~/config/vs-code/settings.json ~/.config/Code/User/settings.json

# vscode keybindings symlink
rm ~/.config/Code/User/keybindings.json
ln -s ~/config/vs-code/keybindings.json ~/.config/Code/User/keybindings.json

# bashrc symlink
rm ~/.bashrc
ln -s ~/config/bashrc ~/.bashrc

# Install Vim on your VSCode
code --install-extension vscodevim.vim
